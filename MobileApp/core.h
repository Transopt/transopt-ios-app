//
//  core.h
//  mobileapp
//
//  Created by Евгений Малаховский on 27.09.2018.
//  Copyright © 2018 Евгений Малаховский. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "MAScriptMessageHandler.h"

@interface core : NSObject

+(core*)shared;

+(NSString*) deviceType;
+(NSString*) device_id;
+(NSString*) serverName;

-(void)getGPS:(void(^)(CLLocation* s))block error:(void(^)(NSString*))blockfail continueUpdate:(double)distance;

@property NSString *serverPush;
-(void)changeServerPush:(NSString *)serverPush;

-(void)startBackgroundGPS;

-(void)sendServer:(NSString*)url data:(NSString*)data;
-(void)sendServer:(NSString*)url data:(NSString*)data completionHandler:(void (^)(NSDictionary * _Nullable responseJson))completionHandler;

-(void)setColors:(UIViewController *)ctrl;
-(UIViewController*)rootController;

@property BOOL isGPSBackground;

@property WKWebsiteDataStore *websiteDataStore;

-(void)changeActiveGPS:(UIApplication*)app;
@end
