//
//  AppDelegate.m
//  AppForSale2
//
//  Created by Евгений Малаховский on 23.08.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "DrawerController.h"
#import "LeftButton.h"
#import "core.h"

@import Firebase;
#import <UserNotifications/UserNotifications.h>

@implementation AppDelegate

-(void)onMenu{
        
}

-(void)getTokenPush{
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        //[UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             [self sendServerToken:[[FIRInstanceID instanceID] token]];
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
}

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    [FIRMessaging messaging].delegate = self;
    [FIRApp configure];
    
    if ([UNUserNotificationCenter class] != nil){
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    }
    
    [self getTokenPush];
    
    [[core shared] startBackgroundGPS];
    application.applicationIconBadgeNumber = 0;
    
    return YES;
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler __IOS_AVAILABLE(10.0) __TVOS_AVAILABLE(10.0) __WATCHOS_AVAILABLE(3.0){
    
    UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
    UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
    
    completionHandler(authOptions);
}
    
- (void)applicationDidBecomeActive:(UIApplication *)application{
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    application.applicationIconBadgeNumber = 0;
    [[core shared] changeActiveGPS:application];
}
    
- (void)applicationDidEnterBackground:(UIApplication *)application{
    [[core shared] changeActiveGPS:application];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
    
- (void)applicationWillTerminate:(UIApplication *)application{
    [[NSUserDefaults standardUserDefaults] synchronize];
}
    
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    [self didReceivePush:userInfo];
}
    
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler{
    [self didReceivePush:userInfo];
    handler(UIBackgroundFetchResultNewData);
}

-(void) didReceivePush:(NSDictionary*)userInfo{
    NSString *url = userInfo[@"url"];
    
    if ([UIApplication sharedApplication].applicationState==UIApplicationStateActive){
        ViewController *curVC = [[core shared] rootController];
        if ([curVC.webView.URL.absoluteString isEqualToString:url]){ // Переходить не нужно
            [[NSNotificationCenter defaultCenter] postNotificationName:@"onCustomEvent" object:userInfo[@"functName"]];
        }
        else{ // Спросим нужно ли переходить
            [self pushController:url];
        }
    }
    else{
        [self pushController:url];
    }
}
    
-(void)pushController:(NSString*)url{
    UINavigationController *navCtrl = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"Center"];
    ViewController *nextController = navCtrl.viewControllers[0];
    [[core shared] setColors:nextController];
    nextController.url = url;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goPushMenu" object:navCtrl];
}

-(void)sendServerToken:(NSString*)token{
    NSString *oldToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"oldTokenPush"];
    if ([oldToken isEqualToString:token])
        return;
    
    NSDictionary *setting = [NSDictionary dictionaryWithContentsOfFile:
                             [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"]];
    
    [[core shared] sendServer:
    [[core serverName] stringByAppendingString:@"/bitrix/tools/mlab_appforsale/register_device.php"]
                     data:[NSString stringWithFormat:                                                              @"app_id=%@&device_id=%@&device_model=%@&system_version=%@&token=%@"
                           ,setting[@"CFBundleIdentifier"]
                           ,[core device_id]
                           ,[core deviceType]
                           ,[[UIDevice currentDevice] systemVersion]
                           ,token
                           ]
        completionHandler:^(NSDictionary * _Nullable responseJson) {
            if ([responseJson[@"response"] integerValue] == 1){
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"oldTokenPush"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    ];
}

-(void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken{
    [self sendServerToken:fcmToken];
}

@end
