//
//  ViewController.m
//  AppForSale2
//
//  Created by Евгений Малаховский on 23.08.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import "ViewController.h"
#define StoryBoard [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]

static NSMutableDictionary *cache;

@interface ViewController ()
@end


@implementation ViewController
@synthesize webView;

-(void)prepareWeb{
    if (!self.webView){
        self.webView = [[MAWebView alloc] initWithFrame:self.viewForWeb.bounds];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [self.leftMenu refresh];
    
    if (!webView)
        webView = [[MAWebView alloc] initWithFrame:self.viewForWeb.bounds];
    else
        [webView setFrame:self.viewForWeb.bounds];

    [self.viewForWeb addSubview:webView];
//    [webView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin];

    webView.alpha = 0;
    
    webView.controller = self;
    [webView loadURL:self.url];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [webView setFrame:self.viewForWeb.bounds];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if (self.navigationController.viewControllers.count>1){
        return YES;
    }
    else{
        return NO;
    }
}
@end
