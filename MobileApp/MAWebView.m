//
//  webView.m
//  AppForSale2
//
//  Created by Евгений Малаховский on 27.08.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import "MAWebView.h"
#import "core.h"

#import "ViewController.h"

#define DOCUMENTS [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define StoryBoard [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]

#import <UserNotifications/UserNotifications.h>
#import <FirebaseInstanceID/FIRInstanceID.h>

//#import <Contacts/Contacts.h>
#import "popupWebView.h"

static WKProcessPool *_pool;

@implementation MAWebView{
    ViewController *nextController;
    UIActivityIndicatorView *indicator;
    NSTimer *timerLoad;
}

-(void)prepareNextController{
    nextController = [StoryBoard instantiateViewControllerWithIdentifier:@"ViewController"];
    [nextController prepareWeb];
}

-(ViewController*)getNextController{
    return nextController;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [indicator setFrame:frame];
    
    [[core shared] setColors:self.controller];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame configuration:self.webConfig];
    
    if (!indicator){
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        indicator.color = [UIColor blackColor];
        indicator.frame = self.frame;
        indicator.hidesWhenStopped=YES;
        
        [self.superview addSubview:indicator];
    }
    
    self.navigationDelegate = self;
    self.UIDelegate = self;
    
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.showsHorizontalScrollIndicator=NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onCustomEvent:)
                                                 name:@"onCustomEvent"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeLeftMenu:)
                                                 name:@"changeLeftMenu"
                                               object:nil];

    return self;
}

-(void)changeLeftMenu:(NSNotification*)obj{
    if (self.isLeftMenu){
        [self loadURL:obj.object];
        [[NSUserDefaults standardUserDefaults] setObject:obj.object forKey:@"changeLeftMenu"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)onCustomEvent:(NSNotification*)notification
{
    NSDictionary *params = notification.object;
    NSString *JS = [NSString stringWithFormat:@"BX.onCustomEvent('%@')", params[@"eventName"]];
    
    if (params[@"params"]){
        NSString *paramsEventStr = [[NSString alloc] initWithData:
                                    [NSJSONSerialization dataWithJSONObject:params[@"params"] options:0 error:nil]
                                                         encoding:NSUTF8StringEncoding];

        JS = [NSString stringWithFormat:@"BX.onCustomEvent('%@', [%@])", params[@"eventName"], paramsEventStr];
    }
    
    [self evaluateJavaScript:JS completionHandler:nil];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - WKNavigationDelegate
- (nullable WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    
    popupWebView *popup = [popupWebView createPopupWebView:self configuration:configuration forNavigationAction:navigationAction windowFeatures:windowFeatures];
        
    return popup.webView;
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    if(webView != self) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    
    UIApplication *app = [UIApplication sharedApplication];
    NSURL         *url = navigationAction.request.URL;
    
    if (!navigationAction.targetFrame) {
        if ([app canOpenURL:url]) {
            [app openURL:url];
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }
    
    if ([url.scheme isEqualToString:@"tel"]){
        if ([app canOpenURL:url]){
            [app openURL:url];
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }
    
    if (![url.scheme hasPrefix:@"http"])
        if ([app canOpenURL:url]){
            [app openURL:url];
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }

    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark - accessors
-(WKWebViewConfiguration*) webConfig{
    if (!_webConfig){
        _webConfig = [[WKWebViewConfiguration alloc]init];
        _webConfig.processPool = [MAWebView pool];
        _webConfig.websiteDataStore = [core shared].websiteDataStore;
        
        WKUserContentController* userController = [WKUserContentController new];
        [userController addScriptMessageHandler:[MAScriptMessageHandler shared] name:@"exec"];
        _webConfig.userContentController = userController;
    }
    return _webConfig;
}

+ (WKProcessPool *)pool{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _pool = [[WKProcessPool alloc] init];
    });
    return _pool;
}

-(void)showIndicator{
    if (!timerLoad)
        return;
    
    [indicator startAnimating];
    [self.superview addSubview:indicator];
}

-(void)hideIndicator{
    if ([timerLoad isValid]){
        [timerLoad invalidate];
        timerLoad = nil;
    }
    
    [indicator stopAnimating];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    self.alpha = 1;
    [UIView commitAnimations];
}

-(void)loadURL:(NSString*)urlStr{
    if (!urlStr){ // Стартовый экран
        NSDictionary *setting = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"]];
        urlStr = setting[@"SiteUrl"];
    }
    
    if (!self.isLeftMenu)
        timerLoad = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(showIndicator) userInfo:nil repeats:NO];
    else
        self.alpha = 1;
    
    NSURL *url;
    url = [NSURL URLWithString:urlStr];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    
    [request setValue:[core device_id] forHTTPHeaderField:@"bm-device-id"];
    [request setValue:[[UIDevice currentDevice] model] forHTTPHeaderField:@"bm-device"];
    [request setValue:@"2" forHTTPHeaderField:@"bm-api-version"];
    
//    if (referer)
//        [request setValue:referer forHTTPHeaderField:@"referer"];
    self.alpha = 0.0f;
    [self loadRequest:request];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    [self hideIndicator];
    [self prepareNextController];
    if (!self.controller.title)
        [self evaluateJavaScript:@"document.title" completionHandler:^(NSString *title, NSError * _Nullable error) {
            if (![title isEqualToString:@""])
                self.controller.title = title;
        }];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    
    NSHTTPURLResponse* response = navigationResponse.response;
    NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:[NSURL URLWithString:@""]];

    if (cookies.count) {
        NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject:cookies];
        [[NSUserDefaults standardUserDefaults] setObject:cookiesData forKey:@"cookies"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    NSAttributedString *tempStr = [[NSAttributedString alloc] initWithData:[message dataUsingEncoding:NSUTF8StringEncoding]
                                             options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                       NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                  documentAttributes:nil error:nil];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:tempStr.string
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler();
                                                      }]];
    [[[core shared] rootController] presentViewController:alertController animated:YES completion:^{}];
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler;{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler(true);
                                                      }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Отмена"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler(false);
                                                      }]];

    [[[core shared] rootController] presentViewController:alertController animated:YES completion:^{}];

}

- (void) presentAlertOnController:(nonnull UIViewController*)parentController title:(nullable NSString*)title message:(nullable NSString *)message handler:(nonnull void (^)())completionHandler{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler();
    }]];
    [parentController presentViewController:alertController animated:YES completion:^{}];
}

- (void) presentPromptOnController:(nonnull UIViewController*)parentController title:(nullable NSString*)title message:(nullable NSString *)message defaultText:(nullable NSString*)defaultText handler:(nonnull void (^)(NSString * __nullable result))completionHandler{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *input = ((UITextField *)alertController.textFields.firstObject).text;
        completionHandler(input);
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler(nil);
    }]];
    [parentController presentViewController:alertController animated:YES completion:^{}];
}

- (void)webView:(WKWebView *)webView
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    CFDataRef exceptions = SecTrustCopyExceptions(serverTrust);
    SecTrustSetExceptions(serverTrust, exceptions);
    CFRelease(exceptions);
    
    completionHandler(NSURLSessionAuthChallengeUseCredential,
                      [NSURLCredential credentialForTrust:serverTrust]);
}

@end
