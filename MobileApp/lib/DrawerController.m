//
//  DrawerController.m
//  LeftMenu
//
//  Created by Евгений Малаховский on 22.02.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import "DrawerController.h"

@interface DrawerController ()

@end

@implementation DrawerController

-(void)goChangeCenter:(NSNotification*)notification{
    [self setCenterViewController:notification.object];
}

-(void)goPushMenu:(NSNotification*)notification{
    [self setCenterViewController:notification.object
          withCloseAnimation:NO
          completion:nil];
}

-(void)goLeftButton{
    [self toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSDictionary *infoStart = [[NSUserDefaults standardUserDefaults] objectForKey:@"infoStart"];
    if (!infoStart)
        infoStart = @{
                    @"menu":@"Left"
                    ,@"center":@"Center"
                    };
    
    self.centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:infoStart[@"center"]];
    
    self.leftDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:infoStart[@"menu"]];
    
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
    [self setMaximumLeftDrawerWidth:([[UIScreen mainScreen] applicationFrame].size.width*0.9)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goLeftButton)
                                                 name:@"goLeftButton"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goPushMenu:)
                                                 name:@"goPushMenu"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goChangeMenu:)
                                                 name:@"goChangeMenu"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goChangeCenter:)
                                                 name:@"goChangeCenter"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OnOffMenu:)
                                                 name:@"OnOffMenu"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeMenu)
                                                 name:@"closeMenu"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableMenu)
                                                 name:@"enableMenu"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toggleMenu)
                                                 name:@"toggleMenu"
                                               object:nil];
}

-(void)enableMenu{
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeBezelPanningCenterView];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
}

-(void)closeMenu{
    [self closeDrawerAnimated:YES completion:nil];
}

-(void)toggleMenu{
    [self openDrawerSide:(MMDrawerSideLeft) animated:YES completion:nil];
}

-(void)OnOffMenu:(NSNotification*)notification{
    BOOL on = [notification.object boolValue];    
    [self setOpenDrawerGestureModeMask:on?MMOpenDrawerGestureModeBezelPanningCenterView:MMOpenDrawerGestureModeNone];
}

-(void)goChangeMenu:(NSNotification*)notification{
    [self setLeftDrawerViewController:notification.object];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
