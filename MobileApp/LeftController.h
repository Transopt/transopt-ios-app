//
//  LeftController.h
//  AppForSale2
//
//  Created by Евгений Малаховский on 13.09.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAWebView.h"

@interface LeftController : UIViewController

@property MAWebView *webView;

@end
