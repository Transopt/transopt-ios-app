//
//  core.m
//  mobileapp
//
//  Created by Евгений Малаховский on 27.09.2018.
//  Copyright © 2018 Евгений Малаховский. All rights reserved.
//

#import "core.h"
#import "KeychainItemWrapper.h"
#import <wchar.h>
#include <sys/utsname.h>
#import "DrawerController.h"

static CLLocationManager *gps;

static void (^GPSblock)(NSDictionary *);
static void (^getGPSblock)(CLLocation *);
static void (^GPSblockfail)(NSString*);

@implementation core

static core *sharedMySingleton = NULL;
+(core*)shared {
    if (!sharedMySingleton) {
        sharedMySingleton = [core new];
        
        NSString *tempServerPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverPush"];
        if (!tempServerPush)
            tempServerPush = [[core serverName] stringByAppendingString:@"/bitrix/tools/mlab_appforsale/register_device.php"];
            
        sharedMySingleton.serverPush = tempServerPush;
        getGPSblock = nil;
        GPSblockfail = nil;
        
        NSDictionary *setting = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"]];
        
        if (setting[@"NSLocationAlwaysAndWhenInUseUsageDescription"])
            sharedMySingleton.isGPSBackground = true;
        
        sharedMySingleton.websiteDataStore = [WKWebsiteDataStore defaultDataStore];
    }
    return sharedMySingleton;
}

-(void)changeActiveGPS:(UIApplication*)app{
    if ([app applicationState]==UIApplicationStateActive) {
        gps.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        gps.distanceFilter = 500;
    } else {
        gps.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        gps.distanceFilter = 500;
    }
}

-(UIViewController*)rootController{
    UIViewController *parentViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    while (parentViewController.presentedViewController != nil){
        parentViewController = parentViewController.presentedViewController;
    }
    
    if ([parentViewController isKindOfClass:[DrawerController class]])
    {
        parentViewController = ((DrawerController*)parentViewController).centerViewController;
    }
    
    UINavigationController *navController = parentViewController;
    if ([navController isKindOfClass:[UINavigationController class]])
    {
        NSArray *temp = navController.viewControllers;
        return (UIViewController *)[navController.viewControllers objectAtIndex:navController.viewControllers.count-1];
    }
    return navController;
}

-(void)changeServerPush:(NSString *)serverPush{
    self.serverPush = serverPush;
    [[NSUserDefaults standardUserDefaults] setObject:self.serverPush forKey:@"serverPush"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString*) device_id{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"device_id" accessGroup:nil];
    NSString *result = [keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
    
    if (result.length==0){
        result = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [keychainItem setObject:result forKey:(__bridge id)(kSecAttrAccount)];
    }
    //NSLog(@"Device_id: %@", result);
    return result;
}

+ (NSString *)deviceType {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

+(NSString*)serverName{
    NSDictionary *setting = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"]];
    NSURL *url = [NSURL URLWithString:setting[@"SiteUrl"]];
    
    return [NSString stringWithFormat:@"%@://%@", url.scheme, url.host];
}

-(void)startBackgroundGPS{
    
    [self shareGPS];
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways){
        [gps startMonitoringSignificantLocationChanges];
        [gps startUpdatingLocation];
        [self changeActiveGPS:[UIApplication sharedApplication]];
    }
}

-(void)shareGPS{
    if (!gps){
        gps = [CLLocationManager new];
        gps.delegate = self;
        [self changeActiveGPS:[UIApplication sharedApplication]];
        
        if (self.isGPSBackground){
            gps.allowsBackgroundLocationUpdates = true;
            gps.pausesLocationUpdatesAutomatically = false;
        }
    }
}

-(void)getGPS:(void(^)(CLLocation* s))block error:(void(^)(NSString*))blockfail continueUpdate:(double)distance{
    //if (!getGPSblock){
        getGPSblock = block;
        GPSblockfail = blockfail;
        
        [self shareGPS];
        
        if (distance>0)
            gps.distanceFilter = distance;
    
        if (self.isGPSBackground)
            [gps requestAlwaysAuthorization];
        else
            [gps requestWhenInUseAuthorization];
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse)
            [gps startUpdatingLocation];
    
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways){
            [gps startUpdatingLocation];
            [gps startMonitoringSignificantLocationChanges];
        }
    
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways){
            // Если слежение уже включено, то вернем сразу местоположение
            gps.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            gps.distanceFilter = 0;
            getGPSblock(gps.location);
        }

   // }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status API_AVAILABLE(ios(4.2), macos(10.7));{
    
    if (gps){
        if (status==kCLAuthorizationStatusAuthorizedWhenInUse)
            [gps startUpdatingLocation];
        else if (status==kCLAuthorizationStatusAuthorizedAlways){
            [gps startMonitoringSignificantLocationChanges];
            [gps startUpdatingLocation];
        }
        else{
            if (GPSblockfail){
                GPSblockfail(@"");
                GPSblockfail = nil;
                getGPSblock = nil;
            }
        }
    }
}


- (void)locationManager:(CLLocationManager *)locationManager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;{
//    if (locationManager.distanceFilter==kCLDistanceFilterNone)
//        [gps stopUpdatingLocation];
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse)
        [gps stopUpdatingLocation];
    
    [self changeActiveGPS:[UIApplication sharedApplication]];
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways){
        NSLog(@"Мониторинг GPS: %f,%f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        NSLog(@"Дистанция: %.0f м", [newLocation distanceFromLocation:oldLocation]);
        NSLog(@"Точность: %.0f/%.0f", newLocation.verticalAccuracy, newLocation.horizontalAccuracy);
        
        [[core shared] sendServer:
         [[core serverName] stringByAppendingString:@"/bitrix/tools/mlab_appforsale/set_current_position.php"]
                             data:[NSString stringWithFormat:                                                              @"longitude=%f&latitude=%f&accuracy=%f&timestamp=%f&device_id=%@"
                                   ,newLocation.coordinate.longitude
                                   ,newLocation.coordinate.latitude
                                   ,newLocation.horizontalAccuracy
                                   ,[newLocation.timestamp timeIntervalSince1970]
                                   ,[core device_id]
                                   ]
         ];
    }
    
    if (getGPSblock){
        getGPSblock(newLocation);
        
        if (locationManager.distanceFilter==kCLDistanceFilterNone)
            getGPSblock=nil;
        return;
    }
}

-(void)sendServer:(NSString*)url data:(NSString*)data{
    [self sendServer:url data:data completionHandler:nil];
}

- (void)getAllCookies:(void (^)(NSArray<NSHTTPCookie *> *))completionHandler{

    dispatch_async(dispatch_get_main_queue(), ^{
        if (@available(iOS 11, *)) {
            [[core shared].websiteDataStore.httpCookieStore getAllCookies:^(NSArray *cookiesArray) {
                completionHandler(cookiesArray);
            }];
        } else {
            NSData *cookiesData = [[NSUserDefaults standardUserDefaults] objectForKey:@"cookies"];
            if ([cookiesData length]) {
                NSArray *cookiesArray = [NSKeyedUnarchiver unarchiveObjectWithData:cookiesData];
                completionHandler(cookiesArray);
            }
        }
    });
}

-(void)sendServer:(NSString*)url data:(NSString*)data completionHandler:(void (^)(NSDictionary * _Nullable responseJson))completionHandler{
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    [self getAllCookies:^(NSArray *cookiesArray) {
        NSURLSession *upLoadSession = [NSURLSession sessionWithConfiguration:config];
        NSURL* downloadTaskURL = [NSURL URLWithString:url];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:downloadTaskURL];
        request.HTTPBody = [data dataUsingEncoding:NSUTF8StringEncoding];
        request.HTTPMethod = @"POST";
        
        NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
        [request setAllHTTPHeaderFields:headers];
        
        [[upLoadSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

            NSError *errorJson;
            NSDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:&errorJson];
            
            if (errorJson)
                responseJson = @{@"error": errorJson.description};
            
            if (completionHandler)
                completionHandler(responseJson);
            
    //        if (![response.URL.absoluteString isEqualToString:downloadTaskURL.absoluteString]){
    //            [[core shared] changeServerPush:response.URL.absoluteString];
    //            [self sendServerToken: token];
    //        }
        }] resume];
    }];
}

+ (UIColor*) colorWithHexString: (NSString *) hexString{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat red, blue, green;
    
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: 1.0f];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    unsigned hexComponent;
    [[NSScanner scannerWithString: substring] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

-(void)setColors:(UIViewController *)ctrl{
    NSDictionary *params = [[NSUserDefaults standardUserDefaults] objectForKey:@"setColors"];
    if (!params)
        return;
    
    NSString *titleText = params[@"titleText"];
    NSString *background = params[@"background"];
    
    [self rootController].navigationController.navigationBar.barTintColor = [core colorWithHexString:background];
    [self rootController].navigationController.navigationBar.titleTextAttributes = @{
         UITextAttributeFont                 :  [UIFont fontWithName:@"Helvetica-bold" size:16.0],
         UITextAttributeTextColor            :  [core colorWithHexString:titleText],
         UITextAttributeTextShadowColor      :  [UIColor clearColor],
         UITextAttributeTextShadowOffset     :  [NSValue valueWithUIOffset:UIOffsetZero]
     };
    ctrl.navigationController.navigationBar.barTintColor = [core colorWithHexString:background];
    
    ctrl.navigationController.navigationBar.titleTextAttributes = @{
        UITextAttributeFont                 :  [UIFont fontWithName:@"Helvetica-bold" size:16.0],
        UITextAttributeTextColor            :  [core colorWithHexString:titleText],
        UITextAttributeTextShadowColor      :  [UIColor clearColor],
        UITextAttributeTextShadowOffset     :  [NSValue valueWithUIOffset:UIOffsetZero]
    };
}

@end
